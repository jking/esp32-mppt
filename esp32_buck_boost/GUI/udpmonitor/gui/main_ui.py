# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main_ui.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1095, 681)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.formLayout = QtWidgets.QFormLayout(self.centralwidget)
        self.formLayout.setObjectName("formLayout")
        self.groupBox_3 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_3.setObjectName("groupBox_3")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.groupBox_3)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.horizontalLayout_1 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_1.setObjectName("horizontalLayout_1")
        self.label_10 = QtWidgets.QLabel(self.groupBox_3)
        self.label_10.setObjectName("label_10")
        self.horizontalLayout_1.addWidget(self.label_10)
        self.wifi_command_lineEdit_1 = QtWidgets.QLineEdit(self.groupBox_3)
        self.wifi_command_lineEdit_1.setObjectName("wifi_command_lineEdit_1")
        self.horizontalLayout_1.addWidget(self.wifi_command_lineEdit_1)
        self.wifi_command_pushButton_1 = QtWidgets.QPushButton(self.groupBox_3)
        self.wifi_command_pushButton_1.setObjectName("wifi_command_pushButton_1")
        self.horizontalLayout_1.addWidget(self.wifi_command_pushButton_1)
        self.gridLayout_2.addLayout(self.horizontalLayout_1, 0, 0, 1, 1)
        self.groupBox_5 = QtWidgets.QGroupBox(self.groupBox_3)
        self.groupBox_5.setObjectName("groupBox_5")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupBox_5)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.groupBox_8 = QtWidgets.QGroupBox(self.groupBox_5)
        self.groupBox_8.setObjectName("groupBox_8")
        self.gridLayout_10 = QtWidgets.QGridLayout(self.groupBox_8)
        self.gridLayout_10.setObjectName("gridLayout_10")
        self.label_6 = QtWidgets.QLabel(self.groupBox_8)
        self.label_6.setObjectName("label_6")
        self.gridLayout_10.addWidget(self.label_6, 0, 0, 1, 1)
        self.label_9 = QtWidgets.QLabel(self.groupBox_8)
        self.label_9.setObjectName("label_9")
        self.gridLayout_10.addWidget(self.label_9, 0, 1, 1, 1)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label = QtWidgets.QLabel(self.groupBox_8)
        self.label.setObjectName("label")
        self.horizontalLayout_6.addWidget(self.label)
        self.label_3 = QtWidgets.QLabel(self.groupBox_8)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_6.addWidget(self.label_3)
        self.gridLayout_10.addLayout(self.horizontalLayout_6, 0, 2, 1, 1)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.label_7 = QtWidgets.QLabel(self.groupBox_8)
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_7.addWidget(self.label_7)
        self.label_8 = QtWidgets.QLabel(self.groupBox_8)
        self.label_8.setObjectName("label_8")
        self.horizontalLayout_7.addWidget(self.label_8)
        self.gridLayout_10.addLayout(self.horizontalLayout_7, 0, 3, 1, 1)
        self.verticalLayout_2.addWidget(self.groupBox_8)
        self.groupBox_7 = QtWidgets.QGroupBox(self.groupBox_5)
        self.groupBox_7.setObjectName("groupBox_7")
        self.gridLayout_9 = QtWidgets.QGridLayout(self.groupBox_7)
        self.gridLayout_9.setObjectName("gridLayout_9")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_5 = QtWidgets.QLabel(self.groupBox_7)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_2.addWidget(self.label_5)
        self.pwmc_label = QtWidgets.QLabel(self.groupBox_7)
        self.pwmc_label.setObjectName("pwmc_label")
        self.horizontalLayout_2.addWidget(self.pwmc_label)
        self.gridLayout_9.addLayout(self.horizontalLayout_2, 0, 0, 1, 1)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_2 = QtWidgets.QLabel(self.groupBox_7)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_4.addWidget(self.label_2)
        self.pwm_label = QtWidgets.QLabel(self.groupBox_7)
        self.pwm_label.setObjectName("pwm_label")
        self.horizontalLayout_4.addWidget(self.pwm_label)
        self.gridLayout_9.addLayout(self.horizontalLayout_4, 0, 1, 1, 1)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_4 = QtWidgets.QLabel(self.groupBox_7)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_3.addWidget(self.label_4)
        self.ppwm_label = QtWidgets.QLabel(self.groupBox_7)
        self.ppwm_label.setObjectName("ppwm_label")
        self.horizontalLayout_3.addWidget(self.ppwm_label)
        self.gridLayout_9.addLayout(self.horizontalLayout_3, 0, 2, 1, 1)
        self.verticalLayout_2.addWidget(self.groupBox_7)
        self.gridLayout_2.addWidget(self.groupBox_5, 1, 0, 1, 1)
        self.groupBox_6 = QtWidgets.QGroupBox(self.groupBox_3)
        self.groupBox_6.setObjectName("groupBox_6")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.groupBox_6)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_10.setSpacing(1)
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.label_11 = QtWidgets.QLabel(self.groupBox_6)
        self.label_11.setObjectName("label_11")
        self.horizontalLayout_10.addWidget(self.label_11)
        self.wifi_IP_lineEdit_3 = QtWidgets.QLineEdit(self.groupBox_6)
        self.wifi_IP_lineEdit_3.setObjectName("wifi_IP_lineEdit_3")
        self.horizontalLayout_10.addWidget(self.wifi_IP_lineEdit_3)
        self.gridLayout_7.addLayout(self.horizontalLayout_10, 1, 0, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(1)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_12 = QtWidgets.QLabel(self.groupBox_6)
        self.label_12.setObjectName("label_12")
        self.horizontalLayout.addWidget(self.label_12)
        self.wifi_IP_lineEdit = QtWidgets.QLineEdit(self.groupBox_6)
        self.wifi_IP_lineEdit.setText("")
        self.wifi_IP_lineEdit.setObjectName("wifi_IP_lineEdit")
        self.horizontalLayout.addWidget(self.wifi_IP_lineEdit)
        self.wifi_config_pushButton = QtWidgets.QPushButton(self.groupBox_6)
        self.wifi_config_pushButton.setObjectName("wifi_config_pushButton")
        self.horizontalLayout.addWidget(self.wifi_config_pushButton)
        self.gridLayout_7.addLayout(self.horizontalLayout, 2, 0, 1, 1)
        self.horizontalLayout_21 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_21.setObjectName("horizontalLayout_21")
        self.label_20 = QtWidgets.QLabel(self.groupBox_6)
        self.label_20.setObjectName("label_20")
        self.horizontalLayout_21.addWidget(self.label_20)
        self.SAMPLE_doubleSpinBox = QtWidgets.QDoubleSpinBox(self.groupBox_6)
        self.SAMPLE_doubleSpinBox.setDecimals(0)
        self.SAMPLE_doubleSpinBox.setMinimum(300.0)
        self.SAMPLE_doubleSpinBox.setMaximum(10000.0)
        self.SAMPLE_doubleSpinBox.setSingleStep(100.0)
        self.SAMPLE_doubleSpinBox.setStepType(QtWidgets.QAbstractSpinBox.AdaptiveDecimalStepType)
        self.SAMPLE_doubleSpinBox.setProperty("value", 300.0)
        self.SAMPLE_doubleSpinBox.setObjectName("SAMPLE_doubleSpinBox")
        self.horizontalLayout_21.addWidget(self.SAMPLE_doubleSpinBox)
        self.gridLayout_7.addLayout(self.horizontalLayout_21, 0, 0, 1, 1)
        self.gridLayout_2.addWidget(self.groupBox_6, 5, 0, 1, 1)
        self.groupBox_4 = QtWidgets.QGroupBox(self.groupBox_3)
        self.groupBox_4.setObjectName("groupBox_4")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.groupBox_4)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.horizontalLayout_20 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_20.setObjectName("horizontalLayout_20")
        self.label_19 = QtWidgets.QLabel(self.groupBox_4)
        self.label_19.setObjectName("label_19")
        self.horizontalLayout_20.addWidget(self.label_19)
        self.INCD_doubleSpinBox = QtWidgets.QDoubleSpinBox(self.groupBox_4)
        self.INCD_doubleSpinBox.setSingleStep(0.5)
        self.INCD_doubleSpinBox.setProperty("value", 10.0)
        self.INCD_doubleSpinBox.setObjectName("INCD_doubleSpinBox")
        self.horizontalLayout_20.addWidget(self.INCD_doubleSpinBox)
        self.INCD_pushButton = QtWidgets.QPushButton(self.groupBox_4)
        self.INCD_pushButton.setObjectName("INCD_pushButton")
        self.horizontalLayout_20.addWidget(self.INCD_pushButton)
        self.gridLayout_5.addLayout(self.horizontalLayout_20, 4, 0, 1, 1)
        self.horizontalLayout_18 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_18.setObjectName("horizontalLayout_18")
        self.label_17 = QtWidgets.QLabel(self.groupBox_4)
        self.label_17.setObjectName("label_17")
        self.horizontalLayout_18.addWidget(self.label_17)
        self.RNFA_doubleSpinBox = QtWidgets.QDoubleSpinBox(self.groupBox_4)
        self.RNFA_doubleSpinBox.setDecimals(4)
        self.RNFA_doubleSpinBox.setMaximum(1.0)
        self.RNFA_doubleSpinBox.setSingleStep(0.001)
        self.RNFA_doubleSpinBox.setProperty("value", 0.002)
        self.RNFA_doubleSpinBox.setObjectName("RNFA_doubleSpinBox")
        self.horizontalLayout_18.addWidget(self.RNFA_doubleSpinBox)
        self.RNFA_pushButton = QtWidgets.QPushButton(self.groupBox_4)
        self.RNFA_pushButton.setObjectName("RNFA_pushButton")
        self.horizontalLayout_18.addWidget(self.RNFA_pushButton)
        self.gridLayout_5.addLayout(self.horizontalLayout_18, 2, 0, 1, 1)
        self.horizontalLayout_17 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_17.setObjectName("horizontalLayout_17")
        self.label_16 = QtWidgets.QLabel(self.groupBox_4)
        self.label_16.setObjectName("label_16")
        self.horizontalLayout_17.addWidget(self.label_16)
        self.INVD_doubleSpinBox = QtWidgets.QDoubleSpinBox(self.groupBox_4)
        self.INVD_doubleSpinBox.setDecimals(3)
        self.INVD_doubleSpinBox.setMaximum(120.0)
        self.INVD_doubleSpinBox.setSingleStep(0.5)
        self.INVD_doubleSpinBox.setProperty("value", 50.0)
        self.INVD_doubleSpinBox.setObjectName("INVD_doubleSpinBox")
        self.horizontalLayout_17.addWidget(self.INVD_doubleSpinBox)
        self.INVD_pushButton = QtWidgets.QPushButton(self.groupBox_4)
        self.INVD_pushButton.setObjectName("INVD_pushButton")
        self.horizontalLayout_17.addWidget(self.INVD_pushButton)
        self.gridLayout_5.addLayout(self.horizontalLayout_17, 0, 0, 1, 1)
        self.horizontalLayout_19 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_19.setObjectName("horizontalLayout_19")
        self.label_18 = QtWidgets.QLabel(self.groupBox_4)
        self.label_18.setObjectName("label_18")
        self.horizontalLayout_19.addWidget(self.label_18)
        self.RNFB_doubleSpinBox = QtWidgets.QDoubleSpinBox(self.groupBox_4)
        self.RNFB_doubleSpinBox.setDecimals(4)
        self.RNFB_doubleSpinBox.setMaximum(1.0)
        self.RNFB_doubleSpinBox.setSingleStep(0.001)
        self.RNFB_doubleSpinBox.setProperty("value", 0.002)
        self.RNFB_doubleSpinBox.setObjectName("RNFB_doubleSpinBox")
        self.horizontalLayout_19.addWidget(self.RNFB_doubleSpinBox)
        self.RNFB_pushButton = QtWidgets.QPushButton(self.groupBox_4)
        self.RNFB_pushButton.setObjectName("RNFB_pushButton")
        self.horizontalLayout_19.addWidget(self.RNFB_pushButton)
        self.gridLayout_5.addLayout(self.horizontalLayout_19, 3, 0, 1, 1)
        self.horizontalLayout_16 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_16.setObjectName("horizontalLayout_16")
        self.label_15 = QtWidgets.QLabel(self.groupBox_4)
        self.label_15.setObjectName("label_15")
        self.horizontalLayout_16.addWidget(self.label_15)
        self.OUTVD_doubleSpinBox = QtWidgets.QDoubleSpinBox(self.groupBox_4)
        self.OUTVD_doubleSpinBox.setDecimals(3)
        self.OUTVD_doubleSpinBox.setSingleStep(0.1)
        self.OUTVD_doubleSpinBox.setProperty("value", 50.0)
        self.OUTVD_doubleSpinBox.setObjectName("OUTVD_doubleSpinBox")
        self.horizontalLayout_16.addWidget(self.OUTVD_doubleSpinBox)
        self.OUTVD_pushButton = QtWidgets.QPushButton(self.groupBox_4)
        self.OUTVD_pushButton.setObjectName("OUTVD_pushButton")
        self.horizontalLayout_16.addWidget(self.OUTVD_pushButton)
        self.gridLayout_5.addLayout(self.horizontalLayout_16, 1, 0, 1, 1)
        self.gridLayout_2.addWidget(self.groupBox_4, 3, 0, 1, 1)
        self.groupBox_9 = QtWidgets.QGroupBox(self.groupBox_3)
        self.groupBox_9.setObjectName("groupBox_9")
        self.gridLayout_11 = QtWidgets.QGridLayout(self.groupBox_9)
        self.gridLayout_11.setObjectName("gridLayout_11")
        self.SERIAL_pushButton = QtWidgets.QPushButton(self.groupBox_9)
        self.SERIAL_pushButton.setObjectName("SERIAL_pushButton")
        self.gridLayout_11.addWidget(self.SERIAL_pushButton, 1, 2, 1, 1)
        self.ESPRESTART_pushButton = QtWidgets.QPushButton(self.groupBox_9)
        self.ESPRESTART_pushButton.setObjectName("ESPRESTART_pushButton")
        self.gridLayout_11.addWidget(self.ESPRESTART_pushButton, 2, 2, 1, 1)
        self.FWUPDATE_pushButton = QtWidgets.QPushButton(self.groupBox_9)
        self.FWUPDATE_pushButton.setObjectName("FWUPDATE_pushButton")
        self.gridLayout_11.addWidget(self.FWUPDATE_pushButton, 3, 2, 1, 1)
        self.gridLayout_2.addWidget(self.groupBox_9, 4, 0, 1, 1)
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.groupBox_3)
        self.groupBox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName("groupBox_2")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.groupBox_2)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.gridLayout_6 = QtWidgets.QGridLayout()
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.raw_pushButton = QtWidgets.QPushButton(self.groupBox_2)
        self.raw_pushButton.setObjectName("raw_pushButton")
        self.gridLayout_6.addWidget(self.raw_pushButton, 0, 1, 1, 1)
        self.wave_pushButton = QtWidgets.QPushButton(self.groupBox_2)
        self.wave_pushButton.setObjectName("wave_pushButton")
        self.gridLayout_6.addWidget(self.wave_pushButton, 0, 0, 1, 1)
        self.gridLayout_4.addLayout(self.gridLayout_6, 0, 0, 1, 1)
        self.tool_layout = QtWidgets.QHBoxLayout()
        self.tool_layout.setObjectName("tool_layout")
        self.gridLayout_4.addLayout(self.tool_layout, 2, 0, 1, 1)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout_4.addLayout(self.gridLayout, 1, 0, 1, 1)
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.groupBox_2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1095, 23))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.groupBox_3.setTitle(_translate("MainWindow", "控制台"))
        self.label_10.setText(_translate("MainWindow", "命令"))
        self.wifi_command_pushButton_1.setText(_translate("MainWindow", "发送"))
        self.groupBox_5.setTitle(_translate("MainWindow", "状态"))
        self.groupBox_8.setTitle(_translate("MainWindow", "运行信息"))
        self.label_6.setText(_translate("MainWindow", "BUCK"))
        self.label_9.setText(_translate("MainWindow", "防逆流"))
        self.label.setText(_translate("MainWindow", "驱动"))
        self.label_3.setText(_translate("MainWindow", "EN"))
        self.label_7.setText(_translate("MainWindow", "错误信息"))
        self.label_8.setText(_translate("MainWindow", "0"))
        self.groupBox_7.setTitle(_translate("MainWindow", "PWM值"))
        self.label_5.setText(_translate("MainWindow", "PWMC:"))
        self.pwmc_label.setText(_translate("MainWindow", "0"))
        self.label_2.setText(_translate("MainWindow", "PWM"))
        self.pwm_label.setText(_translate("MainWindow", "0"))
        self.label_4.setText(_translate("MainWindow", "PPWM:"))
        self.ppwm_label.setText(_translate("MainWindow", "0"))
        self.groupBox_6.setTitle(_translate("MainWindow", "连接"))
        self.label_11.setText(_translate("MainWindow", "目标IP"))
        self.wifi_IP_lineEdit_3.setText(_translate("MainWindow", "192.168.5.63"))
        self.label_12.setText(_translate("MainWindow", "本机IP"))
        self.wifi_config_pushButton.setText(_translate("MainWindow", "设置"))
        self.label_20.setText(_translate("MainWindow", "图表采样数"))
        self.groupBox_4.setTitle(_translate("MainWindow", "参数校准"))
        self.label_19.setText(_translate("MainWindow", "ADS712电流校准"))
        self.INCD_pushButton.setText(_translate("MainWindow", "发送"))
        self.label_17.setText(_translate("MainWindow", "输入采样电阻"))
        self.RNFA_pushButton.setText(_translate("MainWindow", "发送"))
        self.label_16.setText(_translate("MainWindow", "校准输入电压"))
        self.INVD_pushButton.setText(_translate("MainWindow", "发送"))
        self.label_18.setText(_translate("MainWindow", "输出采样电阻"))
        self.RNFB_pushButton.setText(_translate("MainWindow", "发送"))
        self.label_15.setText(_translate("MainWindow", "校准输出电压"))
        self.OUTVD_pushButton.setText(_translate("MainWindow", "发送"))
        self.groupBox_9.setTitle(_translate("MainWindow", "功能"))
        self.SERIAL_pushButton.setText(_translate("MainWindow", "串口信息设置"))
        self.ESPRESTART_pushButton.setText(_translate("MainWindow", "重启ESP32"))
        self.FWUPDATE_pushButton.setText(_translate("MainWindow", "升级固件"))
        self.groupBox_2.setTitle(_translate("MainWindow", "可视化图表"))
        self.raw_pushButton.setText(_translate("MainWindow", "原始数据"))
        self.wave_pushButton.setText(_translate("MainWindow", "波形图"))
